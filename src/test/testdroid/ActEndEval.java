package test.testdroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.TextView;

public class ActEndEval extends Activity {
		protected DBSendUser send;
		protected DBGet connect;
		protected int user_id=0;
		EditText inputPassword;
		String originalPassword;
		
		protected void onCreate(Bundle savedInstanceState) 
		{
			super.onCreate(savedInstanceState);
			setContentView(R.layout.end_eval);
			

			try
			{
				Intent tent = getIntent();
				user_id = tent.getIntExtra("userID", user_id);	
				
				findViewById(R.id.button1).setOnTouchListener(touchListen);
				TextView message = (TextView) findViewById(R.id.registerMessage);
				message.setText("Thank you for your answers! The button will send you back to the list.");
			}
			catch(Exception e)
			{
				Log.e("EndValRor", e.toString());
			}
		}

		public boolean onCreateOptionsMenu(Menu menu) 
		{
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}

		public boolean onOptionsItemSelected(MenuItem item) 
		{
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}

		OnTouchListener touchListen = new OnTouchListener() 
		{
			
			public boolean onTouch(View v, MotionEvent e) 
			{
				v.performClick();
				Intent nextScreen;
				nextScreen = new Intent(getApplicationContext(), ActEvals.class);
				nextScreen.putExtra("userID", user_id);
				startActivity(nextScreen);
				return true;
			}
		};
}
