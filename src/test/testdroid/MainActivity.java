package test.testdroid;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements DBReturn{
public static final String PREFS_NAME = "TestDroidPrefsFile";
private static final String PREF_USERNAME = "email";
private static final String PREF_PASSWORD = "password";

protected DBGet connect;
protected EditText inputEmail;
protected EditText inputPassword;
//private String betterString = "7A72CD9A1CDB32CE4DBA1748D8183";


	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		SharedPreferences pref = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);   
		String usernamePref = pref.getString(PREF_USERNAME, "Email");
		String passwordPref = pref.getString(PREF_PASSWORD, "");

		findViewById(R.id.button1).setOnTouchListener(touchListen);
		findViewById(R.id.button2).setOnTouchListener(touchListen);
		
		inputEmail = (EditText) findViewById(R.id.inputEmail);
		inputPassword = (EditText) findViewById(R.id.inputPassword);
		inputEmail.setText(usernamePref);
		inputPassword.setText(passwordPref);
		
		TextView email = (TextView) findViewById(R.id.email);
		TextView password = (TextView) findViewById(R.id.password);
		TextView register = (TextView) findViewById(R.id.register);
		TextView login = (TextView) findViewById(R.id.login);
		email.setText("Email: ");
		password.setText("Password: ");	
		register.setText("Registreer");
		login.setText("Login");
		
		connect = new DBGet(this);
	}

	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	OnTouchListener touchListen = new OnTouchListener() 
	{
		public boolean onTouch(View v, MotionEvent e) 
		{
			Intent nextScreen;
			String email=inputEmail.getText().toString();
			String password=inputPassword.getText().toString();
			
			/*try 
			{
				password=CryptModule.encrypt(betterString, password);
			} 
			catch (GeneralSecurityException e1) 
			{
				Log.e("encryptror", e1.toString());
			}*/
			
			switch(v.getId())
			{
			case R.id.button1:
				v.performClick();
				v.setAlpha(0);
				v.setEnabled(false);
				nextScreen = new Intent(getApplicationContext(), ActRegister.class);
				nextScreen.putExtra("email", email);
				nextScreen.putExtra("password", password);
				startActivity(nextScreen);

			break;
			case R.id.button2:
				v.performClick();
				v.setAlpha(0);
				v.setEnabled(false);
				//Log.i("Connecting with: ","http://ipsen5.westerhoud.nl/api/user/"+email+"/"+password);
				connect.getJSONfromURL("login","http://ipsen5.westerhoud.nl/api/user/"+email+"/"+password);
			break;
			default:
			throw new RuntimeException("Unknow button ID");
			}
			return true;
		}
	};
	
	public void loginFinish(String output)
	{
		//Log.i("Data", output);
		int userID=0;
		
		try 
		{
			JSONObject raw = new JSONObject(output);
			JSONObject data = (JSONObject) raw.get("data");
			userID = Integer.parseInt(data.get("id").toString());
		} 
		catch (Exception e) 
		{
			Log.e("Userror", e.toString());
		}
		
		
		if( ! output.equals("Not found.") )
		{
			SharedPreferences prefs = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
			Editor editor=prefs.edit();
	        editor.putString(PREF_USERNAME, inputEmail.getText().toString());
	        editor.putString(PREF_PASSWORD, inputPassword.getText().toString());
	        editor.commit();
			inputEmail.getText();
			
			Intent nextScreen;
			nextScreen = new Intent(getApplicationContext(), ActEvals.class);
			nextScreen.putExtra("userID", userID);
			startActivity(nextScreen);
		}
		else
		{
			findViewById(R.id.button2).setAlpha(1);
			findViewById(R.id.button2).setEnabled(true);
		}
	}

	public void evalFinish(String output) 
	{
		//interface method
	}

	public void defaultFinish(String output) 
	{
		//interface method
	}
}
