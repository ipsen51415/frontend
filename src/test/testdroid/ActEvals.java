package test.testdroid;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

public class ActEvals extends Activity implements DBReturn{
protected DBGet connect;
protected LinearLayout listView;
protected ArrayList<Button> eval;
protected JSONArray data;
protected int uid;

	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose);

		Intent tent = getIntent();
		uid = tent.getIntExtra("userID", 0);
		Log.i("uid", ""+ uid);
		
		listView = (LinearLayout) findViewById(R.id.listContainer);
		eval = new ArrayList<>();
		data = new JSONArray();
				
		connect = new DBGet(this);
		connect.getJSONfromURL("default","http://ipsen5.westerhoud.nl/api/link/"+uid);
	}

	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	OnClickListener clickListen = new OnClickListener() 
	{
		public void onClick(View v) 
		{
			//v.performClick();
			//v.setAlpha(0);
			//v.setEnabled(false);
			Button clicked = (Button) v;
			int evalNumber = Integer.parseInt(clicked.getTag().toString());

			try{
				int i=0;
				while(!data.isNull(i))
				{					
					JSONObject evaluation = (JSONObject)(data.get(i));
					int evalCompare = Integer.parseInt(evaluation.get("id").toString());
					if(evalNumber == evalCompare)
					{
						connect.getJSONfromURL("eval","http://ipsen5.westerhoud.nl/api/link/question/"+evalNumber);
					}
					i++;
				}
			}
			catch(Exception e)
			{
				Log.e("clickror", e.toString());
			}
		}
	};
	
	public void onBackPressed () {
	    moveTaskToBack (true);
	}
	
	
	public void loginFinish(String output)
	{
		//interface method
	}

	public void evalFinish(String output) 
	{
		JSONObject putout=null;
		JSONArray questionData=null;
		
		Log.i("Data", output);
		try
		{
			putout = new JSONObject(output);
			questionData = (JSONArray) putout.get("data");
			
			if(!questionData.isNull(0))
			{
				Intent nextScreen;
				JSONObject firstQuestion = (JSONObject) questionData.get(0);
				String qType =  firstQuestion.get("type").toString();
				switch(qType)
				{
					case "janee":
						nextScreen = new Intent(getApplicationContext(), ActQ1.class);
						nextScreen.putExtra("questionData", questionData.toString());
						nextScreen.putExtra("questionID", 0);
						nextScreen.putExtra("userID", uid);
						startActivity(nextScreen);
						break;
					case "ster":
						nextScreen = new Intent(getApplicationContext(), ActQ2.class);
						nextScreen.putExtra("questionData", questionData.toString());
						nextScreen.putExtra("questionID", 0);
						nextScreen.putExtra("userID", uid);
						startActivity(nextScreen);
						break;
					default:
						break;
				}
			}
			else
			{
				Log.e("NAN", "No questions found");
			}
		}
		catch(Exception e)
		{
			Log.e("questror", e.toString());
		}
	}

	public void defaultFinish(String output) 
	{
		Log.i("Data", output);
		try {
			JSONObject dataObject = new JSONObject(output);
			
			data = (JSONArray) dataObject.get("data");
			
			int i=0;
			
			while(!data.isNull(i))
			{
				eval.add(new Button(getBaseContext()));

				Button made = (Button) (eval.get(i));
				JSONObject evaluation = (JSONObject)(data.get(i));
				made.setText("ID: "+evaluation.get("id")+". Creator ID: "+evaluation.get("evaluatie_creator_id"));
				made.setTag(""+evaluation.get("id"));
				made.setOnClickListener(clickListen);

				listView.addView(eval.get(i));
				i++;
			}
			
			//output=data.get(1).toString();
		} 
		catch (JSONException e) 
		{
			Log.e("chooseEval error", e.toString());
		}
	}
}
