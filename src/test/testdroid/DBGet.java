package test.testdroid;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class DBGet 
{
	public static DBReturn DBR=null;
	static JSONObject jArray;
	static String order="no";
	
	public DBGet(DBReturn result) 
	{
		DBR=result;
	}

	public void getJSONfromURL(String order,String urlString) 
	{
	    String result = "";
	    jArray = new JSONObject();
		if(DBGet.order=="no")
		{
		    DataCall call=new DataCall();
		    call.execute(urlString);
		    result=jArray.toString();
			DBGet.order=order;
		    try {
		        jArray = new JSONObject(result);
		    } catch (JSONException e) {
		        Log.e("log_tag", "Error parsing data " + e.toString());
		    }
		}
		else
		{
			Log.e("DBGet error", "received request before finished with the last");
		}
	}
	
    static class DataCall extends AsyncTask<String, Void, String> 
    {
    	
		protected String doInBackground(String... params) 
		{
		    InputStream is = null;
		    String result = "";
		    
			String urlString=params[0];
			
		    // Download JSON data from URL
		    StringBuffer chaine = new StringBuffer("");
		    try {
		        URL url = new URL(urlString);
		        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		        connection.setRequestMethod("GET");
		        connection.connect();

		        is = connection.getInputStream();

		        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		        String line = "";
		        
		        try
		        {
			        // Convert response to string
			        while ((line = rd.readLine()) != null) {
			            chaine.append(line);
			        }
			        result = chaine.toString();	
		        }
		        catch(Exception e)
		        {
		        	Log.e("log_tag", "Error converting http result to string " + e.toString());
		        }

		    } catch (Exception e) {
		        Log.e("log_tag", "Error in http connection " + e.toString());
		        result = "Not found.";
		    }
		    
			return result;
		}
		
	 	protected void onPostExecute(String result) 
	 	{
	 		switch(order)
			{
				case "default":
					DBR.defaultFinish(result);
					break;
				case "login":
			 	    DBR.loginFinish(result);
					break;
				case "eval":
			 	    DBR.evalFinish(result);
					break;
				default:
					break;
			}
	 		DBGet.order="no";
	 	}
    }
}