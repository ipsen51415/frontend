package test.testdroid;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class DBSendAnswer
{
	public static DBReturn DBR=null;
	static JSONObject jArray;
	
	public DBSendAnswer(DBReturn result) 
	{
		DBR=result;
	}

	public JSONObject getJSONfromURL(String urlString, int userID, int linkID, int questionID, int answer, int responseTime){
	    String result = "";
	    jArray = new JSONObject();

	    DataCall call=new DataCall();
	    call.execute(urlString, userID+"", linkID+"", questionID+"", answer+"", responseTime+"");
	    result=jArray.toString();
	    
	    try {
	        jArray = new JSONObject(result);
	    } catch (JSONException e) {
	        Log.e("log_tag", "Error parsing data " + e.toString());
	    }

	    return jArray;
	}
	
    static class DataCall extends AsyncTask<String, Void, String> 
    {
    	
		protected String doInBackground(String... params) 
		{
		    String result = "";
		    
		    String urlString = params[0];
			int userID=Integer.parseInt(params[1]);
			int linkID=Integer.parseInt(params[2]);
			int questionID=Integer.parseInt(params[3]);
			int answer=Integer.parseInt(params[4]);
			int responsetime=Integer.parseInt(params[5]);
			
			StringBuilder postData = new StringBuilder();
			postData.append("user_id="+userID);
			postData.append("&link_id="+linkID);
			postData.append("&question_id="+questionID);
			postData.append("&answer="+answer);
			postData.append("&responsetime="+responsetime);
			
			Log.i("postDataa", postData.toString());
			
		    // Download JSON data from URL
		    try {
		        URL url = new URL(urlString);
		        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		        //connection.setRequestProperty("Authorization", passwordString);
		        connection.setRequestMethod("POST");
	            connection.setDoInput(true);
	            connection.setDoOutput(true);
	            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
	            wr.writeBytes(postData.toString());
	    		wr.flush();
	    		wr.close();
	    		int responseCode = connection.getResponseCode();
	    		Log.i("JSON Post Response", ""+responseCode);
	            
		        try
		        {
			        // Convert response to string
			        result = ""+responseCode;	
		        }
		        catch(Exception e)
		        {
		        	Log.e("log_tag", "Error converting http result to string " + e.toString());
		        }

		    } catch (Exception e) {
		        Log.e("log_tag", "Error in http connection " + e.toString());
		    }
		    
			return result;
		}
		
	 	protected void onPostExecute(String result) 
	 	{
	 	    DBR.loginFinish(result);
	 	}
    }
}