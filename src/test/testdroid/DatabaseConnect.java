package test.testdroid;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class DatabaseConnect 
{
	public static DatabaseResult DBR=null;
	static JSONObject jArray;
	
	public DatabaseConnect(DatabaseResult result) 
	{
		DBR=result;
	}

	public JSONObject getJSONfromURL(String urlString) {
	    String result = "";
	    jArray = new JSONObject();

	    DataCall call=new DataCall();
	    call.execute(urlString);
	    result=jArray.toString();
	    
	    try {
	        jArray = new JSONObject(result);
	    } catch (JSONException e) {
	        Log.e("log_tag", "Error parsing data " + e.toString());
	    }

	    return jArray;
	}
	
    static class DataCall extends AsyncTask<String, Void, String> 
    {
    	
		protected String doInBackground(String... params) 
		{
		    InputStream is = null;
		    String result = "";
		    
			String urlString=params[0];
			
		    // Download JSON data from URL
		    StringBuffer chaine = new StringBuffer("");
		    try {
		        URL url = new URL(urlString);
		        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		        //connection.setRequestProperty("Authorization", passwordString);
		        connection.setRequestMethod("GET");
		        connection.connect();

		        is = connection.getInputStream();

		        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		        String line = "";
		        
		        try
		        {
			        // Convert response to string
			        while ((line = rd.readLine()) != null) {
			            chaine.append(line);
			        }
			        result = chaine.toString();	
		        }
		        catch(Exception e)
		        {
		        	Log.e("log_tag", "Error converting http result to string " + e.toString());
		        }

		    } catch (Exception e) {
		        Log.e("log_tag", "Error in http connection " + e.toString());
		        result = "Not found.";
		    }
		    
			return result;
		}
		
	 	protected void onPostExecute(String result) 
	 	{
	 	    DBR.processFinish(result);
	 	}
    }
}