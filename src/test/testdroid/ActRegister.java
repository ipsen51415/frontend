package test.testdroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.TextView;

public class ActRegister extends Activity implements DBReturn{
	protected DBSendUser send;
	protected DBGet connect;
	protected String email;
	protected String password;
	EditText inputPassword;
	String originalPassword;
	
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		
		Intent i = getIntent();
		email = i.getStringExtra("email");
		password = i.getStringExtra("password");

		TextView emailV = (TextView) findViewById(R.id.email);
		TextView passwordV = (TextView) findViewById(R.id.password);
		emailV.setText("Email: "+email);
		passwordV.setText("Confirm Password: ");	
		
		inputPassword = (EditText) findViewById(R.id.inputPassword);
		findViewById(R.id.button1).setOnTouchListener(touchListen);
		
		//prepare a sender to register the account
		send = new DBSendUser(this);
		
		//check to see if the email is still available
		connect = new DBGet(this);
		connect.getJSONfromURL("login", "http://ipsen5.westerhoud.nl/api/user/"+email);
		
		//we don't want to make this button available until we get a response from the connect above this
		findViewById(R.id.button1).setEnabled(false);
		findViewById(R.id.button1).setAlpha(0);
	}

	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void loginFinish(String output)
	{
		Log.i("loginData", output);
		

		TextView message = (TextView) findViewById(R.id.registerMessage);
		if( ! output.equals("Not found.") )
		{
			message.setText("Email available! Please confirm your password before we commit it to memory.");
			findViewById(R.id.button1).setEnabled(true);
			findViewById(R.id.button1).setAlpha(1);
		}
		else
		{
			message.setText("Email already registered, contact an admin if you've lost access to your account.");
		}
	}

	public void evalFinish(String output) 
	{
		//interface method
	}

	public void defaultFinish(String output) 
	{
		Log.i("defaultData", output);
		Intent nextScreen;
		nextScreen = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(nextScreen);
	}
	
	public void onBackPressed() 
	{
		Intent nextScreen = new Intent(getApplicationContext(), MainActivity.class);
		nextScreen.putExtra("result", "not clear");
		startActivity(nextScreen);
		overridePendingTransition(R.anim.left_in, R.anim.left_out);
	}

	OnTouchListener touchListen = new OnTouchListener() 
	{
		
		public boolean onTouch(View v, MotionEvent e) 
		{
			originalPassword = inputPassword.getText().toString();
			v.performClick();
			v.setAlpha(0);
			v.setEnabled(false);
			if(password.equals(originalPassword))
			{
				send.getJSONfromURL("http://ipsen5.westerhoud.nl/api/user",email,password);
			}
			return true;
		}
	};
}
