package test.testdroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.TextView;

public class Register extends Activity implements DatabaseResult{
	protected DBSendUser send;
	protected String email;
	protected String password;
	EditText inputPassword;
	String originalPassword;
	
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		
		Intent i = getIntent();
		email = i.getStringExtra("email");
		password = i.getStringExtra("password");

		TextView emailV = (TextView) findViewById(R.id.email);
		TextView passwordV = (TextView) findViewById(R.id.password);
		emailV.setText("Email: "+email);
		passwordV.setText("Confirm Password: ");	
		
		inputPassword = (EditText) findViewById(R.id.inputPassword);
		findViewById(R.id.button1).setOnTouchListener(touchListen);
		send = new DBSendUser(this);
	}

	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void processFinish(String output)
	{
		Log.i("Data", output);
		Intent nextScreen;
		nextScreen = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(nextScreen);
	}

	OnTouchListener touchListen = new OnTouchListener() 
	{
		
		public boolean onTouch(View v, MotionEvent e) 
		{
			originalPassword = inputPassword.getText().toString();
			v.performClick();
			v.setAlpha(0);
			v.setEnabled(false);
			if(password.equals(originalPassword))
			{
				send.getJSONfromURL("http://ipsen5.westerhoud.nl/api/user",email,password);
			}
			return true;
		}
	};
}
