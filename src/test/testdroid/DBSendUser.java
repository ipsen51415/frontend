package test.testdroid;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class DBSendUser
{
	public static DBReturn DBR=null;
	static JSONObject jArray;
	
	public DBSendUser(DBReturn result) 
	{
		DBR=result;
	}

	public JSONObject getJSONfromURL(String urlString, String mailString, String passString){
	    String result = "";
	    jArray = new JSONObject();

	    DataCall call=new DataCall();
	    call.execute(urlString, mailString, passString);
	    result=jArray.toString();
	    
	    try {
	        jArray = new JSONObject(result);
	    } catch (JSONException e) {
	        Log.e("log_tag", "Error parsing data " + e.toString());
	    }

	    return jArray;
	}
	
    static class DataCall extends AsyncTask<String, Void, String> 
    {
    	
		protected String doInBackground(String... params) 
		{
		    String result = "";
		    
			String urlString=params[0];
			String mailString=params[1];
			String passString=params[2];
			
			StringBuilder postData = new StringBuilder();
			postData.append("unikey="+"hsl");
			postData.append("&email="+mailString);
			postData.append("&password="+passString);
			
			
		    // Download JSON data from URL
		    try {
		        URL url = new URL(urlString);
		        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		        //connection.setRequestProperty("Authorization", passwordString);
		        connection.setRequestMethod("POST");
	            connection.setDoInput(true);
	            connection.setDoOutput(true);
	            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
	            wr.writeBytes(postData.toString());
	    		wr.flush();
	    		wr.close();
	    		int responseCode = connection.getResponseCode();
	    		Log.i("JSON Post Response", ""+responseCode);
	            
		        try
		        {
			        // Convert response to string
			        result = ""+responseCode;	
		        }
		        catch(Exception e)
		        {
		        	Log.e("log_tag", "Error converting http result to string " + e.toString());
		        }

		    } catch (Exception e) {
		        Log.e("log_tag", "Error in http connection " + e.toString());
		    }
		    
			return result;
		}
		
	 	protected void onPostExecute(String result) 
	 	{
	 	    DBR.defaultFinish(result);
	 	}
    }
}