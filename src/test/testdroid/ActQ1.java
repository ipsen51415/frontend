package test.testdroid;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.Chronometer;
import android.widget.TextView;

public class ActQ1 extends Activity implements DBReturn{
protected DBSendAnswer sender;
protected JSONArray questList;
protected int id=0;
protected int question_id=0;
protected int link_id=0;
protected int user_id=0;
protected Chronometer chron;

	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.type1);
       
		TextView questionView = (TextView) findViewById(R.id.question);

		Intent tent = getIntent();
		chron = new Chronometer(this);
		
		try
		{
			questList = new JSONArray(tent.getStringExtra("questionData"));
			id = tent.getIntExtra("questionID", id);	
			user_id = tent.getIntExtra("userID", 0);	
			
			JSONObject question = (JSONObject) questList.get(id);
			JSONObject link = (JSONObject) question.get("pivot");
			link_id = Integer.parseInt(link.get("link_id").toString());
			question_id = Integer.parseInt(link.get("question_id").toString());
			
			questionView.setText(question.get("question").toString());
			chron.setBase(SystemClock.elapsedRealtime());
			chron.start();
		}
		catch(Exception e)
		{
			Log.e("questTypeRor", e.toString());
		}

		findViewById(R.id.button).setOnTouchListener(touchListen);
		findViewById(R.id.target1).setOnDragListener(dropListen);
		findViewById(R.id.target2).setOnDragListener(dropListen);
		
		sender = new DBSendAnswer(this);
	}

	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/*
	public void onBackPressed() {
			Log.d("CDA", "onBackPressed Called");
			Intent nextScreen = new Intent(getApplicationContext(), MainActivity.class);
			nextScreen.putExtra("result", "not clear");
			startActivity(nextScreen);
			overridePendingTransition(R.anim.left_in, R.anim.left_out);
	}
	*/

	OnTouchListener touchListen = new OnTouchListener() 
	{
		public boolean onTouch(View v, MotionEvent e) 
		{
			v.performClick();
			DragShadow dragShadow =new DragShadow(v);
			ClipData data = ClipData.newPlainText("", "");
			v.setAlpha(0);
			v.setEnabled(false);
			v.startDrag(data, dragShadow, v, 0);
			return true;
		}
		
		
	};
	
	OnDragListener dropListen = new OnDragListener()
	{
		public boolean onDrag (View v, DragEvent e)
		{
			int dragEvent = e.getAction();
			
			switch(dragEvent)
			{
				case DragEvent.ACTION_DRAG_ENTERED:
				break;
				case DragEvent.ACTION_DRAG_EXITED:
				break;
				case DragEvent.ACTION_DROP:
					long elapsedMillis = SystemClock.elapsedRealtime() - chron.getBase();
					switch(v.getId())
					{
						case R.id.target1:
							Log.i("target", "Ja");
							//send yes
							sender.getJSONfromURL("http://ipsen5.westerhoud.nl/api/answer", user_id, link_id, question_id, 1, (int) elapsedMillis);
							chron.stop();
							break;
						case R.id.target2:
							Log.i("target", "Nee");
							//send no
							sender.getJSONfromURL("http://ipsen5.westerhoud.nl/api/answer", user_id, link_id, question_id, 0, (int) elapsedMillis);
							chron.stop();
							break;
						default:
							break;
					}
					
					if(!questList.isNull(id+1))
					{
						try
						{
							Intent nextScreen;
							((View) e.getLocalState()).setAlpha(1);
							((View) e.getLocalState()).setEnabled(true);
							JSONObject nextQuestion = (JSONObject) questList.get(id+1);
							String qType =  nextQuestion.get("type").toString();
							switch(qType)
							{
								case "janee":
									nextScreen = new Intent(getApplicationContext(), ActQ1.class);
									nextScreen.putExtra("questionData", questList.toString());
									nextScreen.putExtra("questionID", id+1);
									nextScreen.putExtra("userID", user_id);
									startActivity(nextScreen);
									break;
								case "ster":
									nextScreen = new Intent(getApplicationContext(), ActQ2.class);
									nextScreen.putExtra("questionData", questList.toString());
									nextScreen.putExtra("questionID", id+1);
									nextScreen.putExtra("userID", user_id);
									startActivity(nextScreen);
									break;
								default:
									break;
							}
						}
						catch(Exception ex)
						{
							Log.e("nextror", ex.toString());
						}
					}
					else
					{
						Log.e("NAN", "No questions found");
						
						sender.getJSONfromURL("http://ipsen5.westerhoud.nl/api/link/user", user_id, link_id, question_id, 1, (int) elapsedMillis);
						
						Intent nextScreen;
						nextScreen = new Intent(getApplicationContext(), ActEndEval.class);
						nextScreen.putExtra("userID", user_id);
						startActivity(nextScreen);
					}
				break;
				case DragEvent.ACTION_DRAG_ENDED:
					if(!e.getResult())
					{
						((View) e.getLocalState()).setAlpha(1);
						((View) e.getLocalState()).setEnabled(true);
						return false;
					}	
				break;
				default:
					
				break;
			}
			
			return true;
		}
	};

	private class DragShadow extends View.DragShadowBuilder 
	{
		public DragShadow(View view) {
			super(view);
		}

		public void onDrawShadow(Canvas canvas) 
		{
			Bitmap shadow = BitmapFactory.decodeResource(getResources(), R.drawable.button_sprite);
			
			//R.id.target1
			//R.id.target2
			
			canvas.drawBitmap(shadow, null, new Rect(0,0,256,256), null);
		}

		public void onProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint) 
		{
			super.onProvideShadowMetrics(shadowSize, shadowTouchPoint);
		}
	}

	public void loginFinish(String output) 
	{
		//interface method
	}

	public void evalFinish(String output) 
	{
		//interface method
	}

	public void defaultFinish(String output) 
	{
		//interface method
	}
}
