package test.testdroid;

public interface DBReturn {

	void defaultFinish(String output);
	void loginFinish(String output);
	void evalFinish(String output);
}
